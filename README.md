# ¿Dónde está mamá Lau?

[Descarga el PDF para impresión](https://gitlab.com/programando-libreros/zines/mama-lau/-/raw/main/pdfs/mama-lau.pdf)

[Descarga el forro para impresión](https://gitlab.com/programando-libreros/zines/mama-lau/-/raw/main/pdfs/mama-lau.forro.pdf)

![](galeria/f01.jpg)
![](galeria/f02.jpg)
![](galeria/f03.jpg)
![](galeria/f04.jpg)
![](galeria/f05.jpg)

## Producción

Para producir el PDF a partir de los archivos de Scribus (SLA) solo requieres dos pasos:

1. En Scribus exportar las páginas como imágenes JPG con calidad y tamaño al 100% y con resolución de 300 dpi en `src/paginas`.
2. Ejecutar el comando `./hacer_pdf` para producir a color o `./hacer_pdf_bn` para producir edición para colorear.

### Requisitos

* ImageMagick
* Ruby
* PDFtk
* Gema RMagick
* [img2booklet](https://gitlab.com/-/snippets/2260086)

## Licencia

Este texto está bajo [Licencia Editorial Abierta y Libre (LEAL)](https://programando.li/bres). Con LEAL eres libre de usar, copiar, reeditar, modificar, distribuir o comercializar bajo las siguientes condiciones:

* Los productos derivados o modificados han de heredar algún tipo de LEAL.
* Los archivos editables y finales habrán de ser de acceso público.
* El contenido no puede implicar difamación, explotación o vigilancia.

